<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnReturnRepository")
 */
class OwnReturn
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OwnPost", inversedBy="ownReturns")
     * @ORM\JoinColumn(nullable=false)
     */
    private $PostRelated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OwnUser", inversedBy="ownReturns")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Author;

    /**
     * @ORM\Column(type="text")
     */
    private $Content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $PublicationDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $EditionDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OwnReturn", inversedBy="ownReturns")
     */
    private $ReturnRelated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OwnReturn", mappedBy="ReturnRelated")
     */
    private $ownReturns;

    public function __construct()
    {
        $this->ownReturns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostRelated(): ?OwnPost
    {
        return $this->PostRelated;
    }

    public function setPostRelated(?OwnPost $PostRelated): self
    {
        $this->PostRelated = $PostRelated;

        return $this;
    }

    public function getAuthor(): ?OwnUser
    {
        return $this->Author;
    }

    public function setAuthor(?OwnUser $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->Content;
    }

    public function setContent(string $Content): self
    {
        $this->Content = $Content;

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->PublicationDate;
    }

    public function setPublicationDate(\DateTimeInterface $PublicationDate): self
    {
        $this->PublicationDate = $PublicationDate;

        return $this;
    }

    public function getEditionDate(): ?\DateTimeInterface
    {
        return $this->EditionDate;
    }

    public function setEditionDate(\DateTimeInterface $EditionDate): self
    {
        $this->EditionDate = $EditionDate;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getReturnRelated(): ?self
    {
        return $this->ReturnRelated;
    }

    public function setReturnRelated(?self $ReturnRelated): self
    {
        $this->ReturnRelated = $ReturnRelated;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getOwnReturns(): Collection
    {
        return $this->ownReturns;
    }

    public function addOwnReturn(self $ownReturn): self
    {
        if (!$this->ownReturns->contains($ownReturn)) {
            $this->ownReturns[] = $ownReturn;
            $ownReturn->setReturnRelated($this);
        }

        return $this;
    }

    public function removeOwnReturn(self $ownReturn): self
    {
        if ($this->ownReturns->contains($ownReturn)) {
            $this->ownReturns->removeElement($ownReturn);
            // set the owning side to null (unless already changed)
            if ($ownReturn->getReturnRelated() === $this) {
                $ownReturn->setReturnRelated(null);
            }
        }

        return $this;
    }
}
