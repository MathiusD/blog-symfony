<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnUserRepository")
 */
class OwnUser implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OwnPost", mappedBy="Author")
     */
    private $ownPosts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OwnReturn", mappedBy="Author")
     */
    private $ownReturns;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    public function __construct()
    {
        $this->ownPosts = new ArrayCollection();
        $this->ownReturns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|OwnPost[]
     */
    public function getOwnPosts(): Collection
    {
        return $this->ownPosts;
    }

    public function addOwnPost(OwnPost $ownPost): self
    {
        if (!$this->ownPosts->contains($ownPost)) {
            $this->ownPosts[] = $ownPost;
            $ownPost->setAuthor($this);
        }

        return $this;
    }

    public function removeOwnPost(OwnPost $ownPost): self
    {
        if ($this->ownPosts->contains($ownPost)) {
            $this->ownPosts->removeElement($ownPost);
            // set the owning side to null (unless already changed)
            if ($ownPost->getAuthor() === $this) {
                $ownPost->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OwnReturn[]
     */
    public function getOwnReturns(): Collection
    {
        return $this->ownReturns;
    }

    public function addOwnReturn(OwnReturn $ownReturn): self
    {
        if (!$this->ownReturns->contains($ownReturn)) {
            $this->ownReturns[] = $ownReturn;
            $ownReturn->setAuthor($this);
        }

        return $this;
    }

    public function removeOwnReturn(OwnReturn $ownReturn): self
    {
        if ($this->ownReturns->contains($ownReturn)) {
            $this->ownReturns->removeElement($ownReturn);
            // set the owning side to null (unless already changed)
            if ($ownReturn->getAuthor() === $this) {
                $ownReturn->setAuthor(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;
        
        return $this;
    }

    /**
     * @return Boolean
     */
    public function IsHerPost(OwnPost $ownPost)
    {
        foreach ($this->getOwnPosts() as $post)
        {
            if ($post->getId() == $ownPost->getId())
            {
                return true;
            }
        }
        return false;
    }
    /**
     * @return Boolean
     */
    public function IsHerReturn(OwnReturn $ownReturn)
    {
        foreach ($this->getOwnReturns() as $return)
        {
            if ($return->getId() == $ownReturn->getId())
            {
                return true;
            }
        }
        return false;
    }
}
