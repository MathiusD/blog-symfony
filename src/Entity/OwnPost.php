<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnPostRepository")
 */
class OwnPost
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OwnUser", inversedBy="ownPosts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Author;

    /**
     * @ORM\Column(type="text")
     */
    private $Content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OwnReturn", mappedBy="PostRelated")
     */
    private $ownReturns;

    /**
     * @ORM\Column(type="datetime")
     */
    private $PublicationDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $EditionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Categorie;

    public function __construct()
    {
        $this->ownReturns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?OwnUser
    {
        return $this->Author;
    }

    public function setAuthor(?OwnUser $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->Content;
    }

    public function setContent(string $Content): self
    {
        $this->Content = $Content;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    /**
     * @return Collection|OwnReturn[]
     */
    public function getOwnReturns(): Collection
    {
        return $this->ownReturns;
    }

    public function addOwnReturn(OwnReturn $ownReturn): self
    {
        if (!$this->ownReturns->contains($ownReturn)) {
            $this->ownReturns[] = $ownReturn;
            $ownReturn->setPostRelated($this);
        }

        return $this;
    }

    public function removeOwnReturn(OwnReturn $ownReturn): self
    {
        if ($this->ownReturns->contains($ownReturn)) {
            $this->ownReturns->removeElement($ownReturn);
            // set the owning side to null (unless already changed)
            if ($ownReturn->getPostRelated() === $this) {
                $ownReturn->setPostRelated(null);
            }
        }

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->PublicationDate;
    }

    public function setPublicationDate(\DateTimeInterface $PublicationDate): self
    {
        $this->PublicationDate = $PublicationDate;

        return $this;
    }

    public function getEditionDate(): ?\DateTimeInterface
    {
        return $this->EditionDate;
    }

    public function setEditionDate(\DateTimeInterface $EditionDate): self
    {
        $this->EditionDate = $EditionDate;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->Categorie;
    }

    public function setCategorie(?string $Categorie): self
    {
        $this->Categorie = $Categorie;

        return $this;
    }
}
