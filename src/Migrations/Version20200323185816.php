<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323185816 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE own_user CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE own_return ADD return_related_id INT DEFAULT NULL, CHANGE post_related_id post_related_id INT DEFAULT NULL, CHANGE author_id author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE own_return ADD CONSTRAINT FK_51042CEC8AF66A33 FOREIGN KEY (return_related_id) REFERENCES own_return (id)');
        $this->addSql('CREATE INDEX IDX_51042CEC8AF66A33 ON own_return (return_related_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE own_return DROP FOREIGN KEY FK_51042CEC8AF66A33');
        $this->addSql('DROP INDEX IDX_51042CEC8AF66A33 ON own_return');
        $this->addSql('ALTER TABLE own_return DROP return_related_id, CHANGE post_related_id post_related_id INT NOT NULL, CHANGE author_id author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE own_user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
