<?php

namespace App\Form;

use App\Entity\OwnReturn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OwnReturnSimplifiedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Content', TextareaType::class)
        ;
        if ($options['return_related'] != [false])
        {
            $builder->add('ReturnRelated', ChoiceType::class, $options['return_related']);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OwnReturn::class,
            'return_related' => [false]
        ]);
        $resolver->setAllowedTypes('return_related', 'array');
    }
}
