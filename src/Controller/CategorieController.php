<?php

namespace App\Controller;

use App\Entity\OwnPost;
use App\Repository\OwnPostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie", name="categorie_index")
     */
    public function index(Request $request)
    {
        $cat = [];
        foreach ($this->getDoctrine()->getManager()->getRepository(OwnPost::class)->findAll() as $ownPost)
        {
            if (!in_array($ownPost->getCategorie(), $cat))
            {
                array_push($cat, $ownPost->getCategorie());
            }
        }
        return $this->render('categorie/index.html.twig', [
            "categories" => $cat
        ]);
    }

    /**
     * @Route("/categorie/{search}", name="categorie_search")
     */
    public function search(OwnPostRepository $repo, String $search)
    {
        return $this->render('categorie/search.html.twig', [
            "ownposts" => $repo->findAllByCategorie($search)
        ]);
    }
}
