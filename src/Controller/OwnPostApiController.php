<?php

namespace App\Controller;

use App\Entity\OwnPost;
use App\Service\OwnPostService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/ownpost")
 */
class OwnPostApiController extends AbstractController
{
    /**
     * @Route("/", name="api_ownpost_index", methods={"GET"})
     */
    public function index(OwnPostService $service) : JsonResponse
    {
        $posts = $service->index();
        
        return new JsonResponse($posts);
    }

    /**
     * @Route("/{id}", name="api_ownpost_show", methods={"GET"})
     */
    public function show(OwnPost $ownPost) : JsonResponse
    {
        return new JsonResponse($ownPost);
    }

}
