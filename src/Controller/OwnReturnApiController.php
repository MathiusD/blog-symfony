<?php

namespace App\Controller;

use App\Entity\OwnReturn;
use App\Entity\OwnPost;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/{ID}/ownreturn")
 */
class OwnReturnApiController extends AbstractController
{
    /**
     * @Route("/", name="api_ownreturn_index", methods={"GET"})
     */
    public function index(Request $request): JsonResponse
    {
        $id = explode("/", $request->getPathInfo())[2];
        return new JsonResponse($this->getDoctrine()->getManager()->getRepository(OwnPost::class)->find($id)->getOwnReturns());
    }


    /**
     * @Route("/{id}", name="ownreturn_show", methods={"GET"})
     */
    public function show(OwnReturn $ownReturn): JsonResponse
    {
        return new JsonResponse($ownReturn);
    }
}
