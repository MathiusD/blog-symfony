<?php

namespace App\Controller;

use App\Entity\OwnPost;
use App\Entity\OwnReturn;
use App\Entity\OwnUser;
use App\Form\OwnReturnType;
use App\Service\OwnReturnService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\OwnReturnSimplifiedType;
use DateTime;

/**
 * @Route("/ownpost/{ID}/ownreturn")
 */
class OwnReturnController extends AbstractController
{
    /**
     * @Route("/", name="ownreturn_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $id = explode("/", $request->getPathInfo())[2];
        return $this->render('ownreturn/index.html.twig', [
            'ownreturns' => $this->getDoctrine()->getManager()->getRepository(OwnPost::class)->find($id)->getOwnReturns(),
            "id" => $id
        ]);
    }

    /**
     * @Route("/new", name="ownreturn_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $route = explode("/", $request->getPathInfo());
        $idpost = $route[2];
        $ownReturn = new OwnReturn();
        $data = $this->getDoctrine()->getManager()->getRepository(OwnPost::class)->find($idpost)->getOwnReturns();
        if ($data == null)
        {
            $options = ['return_related' => false];
        }
        else
        {
            $options = ['return_related' => ["choices" => []]];
            $options['return_related']["choices"]["Nothing"] = null;
            foreach($data as $Return)
            {
                $name =  '"' . $Return->getContent() . '" By : "' . $Return->getEmail() . '"';
                $options['return_related']["choices"][$name] = $Return;
            }
        }
        if ($this->getUser() == null)
        {
            $form = $this->createForm(OwnReturnType::class, $ownReturn, $options);
        }
        else
        {
            $form = $this->createForm(OwnReturnSimplifiedType::class, $ownReturn, $options);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $pass = false;
            if ($this->getUser() == null)
            {
                if (filter_var($ownReturn->getEmail(), FILTER_VALIDATE_EMAIL))
                {
                    $pass = true;
                }
            }
            else
            {
                $ownUser = $this->getUser();
                $ownReturn->setEmail($ownUser->getEmail());
                $ownReturn->setAuthor($ownUser);
                $pass = true;
            }
            if ($pass == true)
            {
                $ownReturn->setPostRelated($entityManager->getRepository(OwnPost::class)->find($idpost));
                if (count($route) > 7)
                {
                    $ownReturn->setReturnRelated($entityManager->getRepository(OwnReturn::class)->find(count($route)[count($route) - 2]));
                }
                $ownReturn->setEditionDate(new DateTime('now'));
                $ownReturn->setPublicationDate(new DateTime('now'));
                $entityManager->persist($ownReturn);
                $entityManager->flush();
    
                return $this->redirectToRoute('ownpost_show',[
                    "id" => $idpost
                ]);
            }
        }
        $error = null;
        if (!filter_var($ownReturn->getEmail(), FILTER_VALIDATE_EMAIL) && strlen($ownReturn->getEmail()) > 0)
        {
            $error = "Mail Incorrect.";
        }

        return $this->render('ownreturn/new.html.twig', [
            'ownreturn' => $ownReturn,
            'form' => $form->createView(),
            'error' => $error,
            'id' => $idpost
        ]);
    }

    /**
     * @Route("/{id}", name="ownreturn_show", methods={"GET"})
     */
    public function show(OwnReturn $ownReturn): Response
    {
        return $this->render('ownreturn/show.html.twig', [
            'ownreturn' => $ownReturn,
            'id' => $ownReturn->getPostRelated()->getId()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ownreturn_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OwnReturn $ownReturn): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->getUser()->IsHerReturn($ownReturn) || $this->isGranted("ADMIN"))
        {
            $form = $this->createForm(OwnReturnSimplifiedType::class, $ownReturn);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $ownReturn->setEditionDate(new DateTime('now'));
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('ownreturn_index',[
                    "ID" => $ownReturn->getPostRelated()->getId()
                ]);
            }

            return $this->render('ownreturn/edit.html.twig', [
                'ownreturn' => $ownReturn,
                'form' => $form->createView(),
                'error' => null,
                'id' => $ownReturn->getPostRelated()->getId()
            ]);
        }
        return $this->redirectToRoute('ownreturn_index',[
            "ID" => $ownReturn->getPostRelated()->getId()
        ]);
    }

    /**
     * @Route("/{id}", name="ownreturn_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OwnReturn $ownReturn): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->getUser()->IsHerReturn($ownReturn) || $this->isGranted("ADMIN"))
        {
            if ($this->isCsrfTokenValid('delete'.$ownReturn->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($ownReturn);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('ownreturn_index', [
            "ID" => $ownReturn->getPostRelated()->getId()
        ]);
    }
}
