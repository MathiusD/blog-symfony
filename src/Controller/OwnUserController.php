<?php

namespace App\Controller;

use App\Entity\OwnUser;
use App\Form\OwnUserType;
use App\Form\OwnUserSimplifiedType;
use App\Repository\OwnUserRepository;
use App\Service\OwnUserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/ownuser")
 */
class OwnUserController extends AbstractController
{
    /**
     * @Route("/", name="ownuser_index", methods={"GET"})
     */
    public function index(OwnUserService $service): Response
    {
        return $this->render('ownuser/index.html.twig', [
            'ownusers' => $service->index(),
        ]);
    }

    /**
     * @Route("/signup", name="signup", methods={"GET","POST"})
     */
    public function new(OwnUserService $service, Request $request, UserPasswordEncoderInterface $userPasswordEncoderInterface): Response
    {
        $ownUser = new OwnUser();
        $form = $this->createForm(OwnUserType::class, $ownUser);
        $form->handleRequest($request);

        $data = $service->new($this, $ownUser, $form, $userPasswordEncoderInterface);

        if ($data['redirect'] != null)
        {
            return $this->redirectToRoute($data['redirect']);
        }

        return $this->render('ownuser/new.html.twig', [
            'ownuser' => $data["User"],
            'form' => $data["form"]->createView(),
            'error' => $data["error"]
        ]);
    }

    /**
     * @Route("/{id}", name="ownuser_show", methods={"GET"})
     */
    public function show(OwnUser $ownUser): Response
    {
        return $this->render('ownuser/show.html.twig', [
            'ownuser' => $ownUser,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ownuser_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OwnUser $ownUser): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->getUser()->getId() == $ownUser->getId())
        {

        }
        $form = $this->createForm(OwnUserSimplifiedType::class, $ownUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && filter_var($ownUser->getEmail(), FILTER_VALIDATE_EMAIL))
        {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ownuser_index');
        }
        $error = null;
        if (!filter_var($ownUser->getEmail(), FILTER_VALIDATE_EMAIL))
        {
            $error = "Mail Incorrect.";
        }
        return $this->render('ownuser/edit.html.twig', [
            'ownuser' => $ownUser,
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/{id}", name="ownuser_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OwnUser $ownUser): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isCsrfTokenValid('delete'.$ownUser->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ownUser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ownuser_index');
    }
}
