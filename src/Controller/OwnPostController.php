<?php

namespace App\Controller;

use App\Entity\OwnPost;
use App\Form\OwnPostType;
use App\Repository\OwnPostRepository;
use App\Service\OwnPostService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class OwnPostController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index(OwnPostService $service): Response
    {
        return $this->render('ownpost/index.html.twig', [
            'ownposts' => $service->index(),
        ]);
    }

    /**
     * @Route("/ownpost/new", name="ownpost_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $ownPost = new OwnPost();
        $form = $this->createForm(OwnPostType::class, $ownPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ownPost->setAuthor($this->getUser());
            $ownPost->setEditionDate(new DateTime('now'));
            $ownPost->setPublicationDate(new DateTime('now'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ownPost);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('ownpost/new.html.twig', [
            'ownpost' => $ownPost,
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/ownpost/{id}", name="ownpost_show", methods={"GET"})
     */
    public function show(OwnPost $ownPost): Response
    {
        return $this->render('ownpost/show.html.twig', [
            'ownpost' => $ownPost,
            'id' => $ownPost->getId()
        ]);
    }

    /**
     * @Route("ownpost/{id}/edit", name="ownpost_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OwnPost $ownPost): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->getUser()->IsHerPost($ownPost) || $this->isGranted("ADMIN"))
        {
            $form = $this->createForm(OwnPostType::class, $ownPost);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $ownPost->setEditionDate(new DateTime('now'));
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('home');
            }

            return $this->render('ownpost/edit.html.twig', [
                'ownpost' => $ownPost,
                'form' => $form->createView(),
                'error' => null
            ]);
        }
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("ownpost/{id}", name="ownpost_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OwnPost $ownPost): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->getUser()->IsHerPost($ownPost) || $this->isGranted("ADMIN"))
        {
            if ($this->isCsrfTokenValid('delete'.$ownPost->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($ownPost);
                $entityManager->flush();
            }
        }
        return $this->redirectToRoute('home');
    }
}
