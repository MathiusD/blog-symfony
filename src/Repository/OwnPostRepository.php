<?php

namespace App\Repository;

use App\Entity\OwnPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OwnPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method OwnPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method OwnPost[]    findAll()
 * @method OwnPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OwnPostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OwnPost::class);
    }

    // /**
    //  * @return OwnPost[] Returns an array of OwnPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OwnPost
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllByCategorie($value): ?array
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.Categorie = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getArrayResult()
        ;
    }
}
