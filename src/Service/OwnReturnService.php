<?php

namespace App\Service;

use App\Entity\OwnReturn;
use Doctrine\ORM\EntityManagerInterface;

class OwnReturnService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Fonction qui récupère tout les OwnReturn de notre site.
     */
    public function index()
    {
        $repo = $this->em->getRepository(OwnReturn::class);

        $returns = $repo->findAll();

        return $returns;
    }
}
