<?php

namespace App\Service;

use App\Entity\OwnPost;
use Doctrine\ORM\EntityManagerInterface;

class OwnPostService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Fonction qui récupère tout les OwnPosts de notre site.
     */
    public function index()
    {
        $repo = $this->em->getRepository(OwnPost::class);

        $posts = $repo->findAll();

        return $posts;
    }
}
