<?php

namespace App\Service;

use App\Entity\OwnUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\OwnUserType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Test\FormInterface;

class OwnUserService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Fonction qui récupère tout les OwnUser de notre site.
     */
    public function index()
    {
        $repo = $this->em->getRepository(OwnUser::class);

        $users = $repo->findAll();

        return $users;
    }

    public function new(AbstractController $context, OwnUser $ownUser, Form $form, UserPasswordEncoderInterface $userPasswordEncoderInterface)
    {
        $redirect = null;
        if ($form->isSubmitted() && $form->isValid() && filter_var($ownUser->getEmail(), FILTER_VALIDATE_EMAIL)) {
            $ownUser->setRoles(['USER']);
            $ownUser->setPassword($userPasswordEncoderInterface->encodePassword($ownUser, $ownUser->getPassword()));
            $this->em->persist($ownUser);
            $this->em->flush();
            $redirect = "home";
        }
        $error = null;
        if (!filter_var($ownUser->getEmail(), FILTER_VALIDATE_EMAIL) && strlen($ownUser->getEmail()) > 0)
        {
            $error = "Mail Incorrect.";
        }
        return ["error" => $error, "form" => $form, "User" => $ownUser, "redirect" => $redirect];
    }
}
