default_target: setup

setup: install
	php bin/console doctrine:migration:migrate

install:
	composer install